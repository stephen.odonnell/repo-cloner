# repo-cloner

Clone or fetch recursively all GitLab or Azure projects within a given namespace, recreating the group/subgroup structure with folders/sub-folders on the filesystem.

## usage
```bash
repo-cloner.py --help

Usage: repo-cloner.py [options]

Options:
  -h, --help                           show this help message and exit
  -u URL, --url=URL                    base URL of the GitLab instance
  -t TOKEN, --token=TOKEN              API token
  -n NAMESPACE, --namespace=NAMESPACE  namespace to clone
  -p PATH, --path=PATH                 destination path for cloned projects
  -b BLACKLIST --blacklist=PATH        path for blacklist file for projects that you do not wish to clone
  -r REPO --repo-provider=REPO         Repo provider. Supported [gitlab, azure]
  --disable-root                       do not create root namepace folder in path
  --dry-run                            list the repositories without clone/fetch
```

## examples

```
./repo-cloner.py --path=~/code/ --token=FOOBAR --namespace=my-gitlab-org --url=https://gitlab.com --repo-provider=gitlab
```

```
/usr/bin/python3 /<pathto>/repo-cloner.py --path=<path> --token=<access-token> --namespace=<namespace> --url=https://gitlab.com --repo-provider=gitlab
```

```
/usr/bin/python3 /<pathto>/repo-cloner.py --path=<path> --token=<access-token> --namespace='<name space>' --url=https://dev.azure.com/<org> --repo-provider=azure
```



### Install python 

  Make sure you install python3 (on Mac and Unbuntu it'll be installed)
  https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-programming-environment-on-an-ubuntu-20-04-server


### Install Python module for Gitlab & Azure

```
  sudo pip3 install --upgrade python-gitlab
  sudo pip3 install --upgrade azure-devops
```

## Note:

This may need to be added to your `PATH`

    /home/sod/.local/bin:


## Pre-reqs for Gitlab

### Setup your ssh key

  https://gitlab.com/-/profile/keys

### Setup an access token

  https://gitlab.com/-/profile/personal_access_tokens

## Pre-reqs for Azure

### Setup your personal access token (PAT)

  https://azure-com/_usersSettings/tokens

### Setup an ssh key

  https://azure-com/_usersSettings/keys

## Notes
Project extended from work done by Michele Tavella <michele.tavella@mtcs.io>