#!/usr/bin/env python
# Michele Tavella <michele.tavella@mtcs.io>
# Stephen O'Donnell <sodonnell80@gmail.com>

from azure.devops.connection import Connection
from msrest.authentication import BasicAuthentication
import pprint

import os
import sys
import re
import shutil
import time
import gitlab
import optparse
import subprocess

if __name__ != "__main__":
    exit(1)


def pname():
    pid = os.getpid()
    return "[repo-cloner," + str(pid) + "]"


parser = optparse.OptionParser("usage: %prog [options]")

parser.add_option(
    "-u",
    "--url",
    dest="url",
    default="https://gitlab.com",
    type="string",
    help="base URL of the Repo provider",
)

parser.add_option(
    "-t", 
    "--token", 
    dest="token", 
    default="", 
    type="string", 
    help="API token"
)

parser.add_option(
    "-r", 
    "--repo-provider", 
    dest="provider", 
    default="gitlab", 
    type="string", 
    help="Repo provider. Supported [gitlab, azure]"
)

parser.add_option(
    "-n",
    "--namespace",
    dest="namespace",
    default="",
    type="string",
    help="namespace to clone",
)

parser.add_option(
    "-b",
    "--blacklist",
    dest="blacklist",
    default=os.getenv("PWD") + "/blacklist.txt",
    type="string",
    help="path for blacklist file for projects that you do not wish to clone",
)

parser.add_option(
    "-p",
    "--path",
    dest="path",
    default=os.getenv("PWD"),
    type="string",
    help="destination path for cloned projects",
)

parser.add_option(
    "--disable-root",
    action="store_true",
    dest="noroot",
    default=False,
    help="do not create root namepace folder in path",
)

parser.add_option(
    "--dry-run",
    action="store_true",
    dest="dryrun",
    default=False,
    help="list the repositories without clone/fetch",
)

(options, args) = parser.parse_args()


# TODO catch errrors
if not os.path.isdir(options.path):
    sys.stderr.write("Error: destination path does not exist " + options.path + "\n")
    exit(1)

git_path = shutil.which("git")
if git_path == "None":
    sys.stderr.write("Error: git executable not installed or not in $PATH" + "\n")
    exit(2)
else:
    print(pname() + " using " + git_path)

t = time.time()

class Container(object):
    pass 

projects = []

if options.provider == "gitlab":
    print("Inspecting Gitlab repo")
    gl = gitlab.Gitlab(options.url, options.token)

    group = gl.groups.get(options.namespace, lazy=True, include_subgroups=True)

    # Get all projects inside the namespace
    for project in group.projects.list(all=True):
        projects.append(project)
        print(pname() + " found " + project.path_with_namespace)

    # Get all projects inside the subgroups
    for group in gl.groups.list(
        all=True, owned=True, query_parameters={"id": options.namespace}
    ):

        for project in group.projects.list(all=True):
            projects.append(project)
            print(pname() + " found " + project.path_with_namespace)

        subgroups = group.subgroups.list(all=True)
        while True:
            for subgroup in subgroups:

                real_group = gl.groups.get(subgroup.id, lazy=True)

                for project in real_group.projects.list(all=True):
                    projects.append(project)
                    print(pname() + " found " + project.path_with_namespace)

                subgroups = real_group.subgroups.list(all=True)

                if len(subgroups) == 0:
                    next

            if len(subgroups) == 0:
                break

if options.provider == "azure":
    print("Inspecting Azure repo")

    # Fill in with your personal access token and org URL
    personal_access_token =  options.token
    organization_url = options.url          

    # Create a connection to the org
    credentials = BasicAuthentication('', personal_access_token)
    connection = Connection(base_url=organization_url, creds=credentials)

    # Get a client (the "core" client provides access to projects, teams, etc)
    core_client = connection.clients.get_core_client()
    # Get git client
    git_client = connection.clients.get_git_client()
            
    # Get the first page of projects
    get_projects_response = core_client.get_projects()

    index = 0
    while get_projects_response is not None:
        for project in get_projects_response.value:
            pprint.pprint("[" + str(index) + "] " + project.name + " : " + project.id)
            if options.namespace == project.name:
                pprint.pprint("Found [" + str(index) + "] " + project.name + " : " + project.id)
                standarise_folder_naming = options.namespace.lower().replace(" ", "_")
                pprint.pprint("Project Folder: " + standarise_folder_naming)   
                repos = git_client.get_repositories(project.id)
                for repo in repos:
                    project = Container()
                    project.id = repo.id
                    project.name = repo.name
                    project.path_with_namespace = standarise_folder_naming + "/" +repo.name
                    project.ssh_url_to_repo = repo.ssh_url
                    projects.append(project)
            index += 1
        if get_projects_response.continuation_token is not None and get_projects_response.continuation_token != "":
            # Get the next page of projects
            get_projects_response = core_client.get_projects(continuation_token=get_projects_response.continuation_token)
        else:
            # All projects have been retrieved
            get_projects_response = None

## Filter blacklist files
blacklist = []
print("Opening blacklist: " + options.blacklist)
with open(options.blacklist) as file_reader:
    for line in file_reader:
        blacklist.append(line.rstrip('\n'))

filtered_projects = projects
for project in projects:
    for removal in blacklist:
        ## print("Test Removing " + removal + " " + project.name)
        if removal.strip() == project.name.strip():
            print("Removing " + project.name)
            filtered_projects.remove(project)


for project in filtered_projects:
    print("Project for Cloning: " + project.name)

if not options.dryrun:
    for project in projects:
        print(pname() + " clone/fetch project " + project.path_with_namespace)
        folders = [f.strip().lower() for f in project.path_with_namespace.split("/")]
        if options.noroot:
            folders.remove(options.namespace)

        mkdir = options.path
        for i in range(len(folders) - 1):
            mkdir = mkdir + "/" + folders[i]
            if not os.path.isdir(mkdir):
                os.mkdir(mkdir)

        clone_path = options.path + "/" + "/".join(str(x) for x in folders)
        clone_path = re.sub("/+", "/", clone_path)
        print(pname() + " folder " + clone_path)
        if not os.path.isdir(clone_path):
            print(pname() + " cloning " + project.ssh_url_to_repo)
            try:
                subprocess.run(["git", "clone", project.ssh_url_to_repo, clone_path])
            except:
                sys.stderr.write("Unexpected error while cloning: terminating\n")
                exit(2)
        else:
            print(pname() + " fetching " + project.ssh_url_to_repo)
            try:
                subprocess.run(["git", "-C", clone_path, "fetch", "--all"])
            except:
                sys.stderr.write("Unexpected error while fetching: terminating\n")
                exit(3)

print(pname() + " mission accomplished in " + str(round(time.time() - t, 2)) + "s")
exit(0)
